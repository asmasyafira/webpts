<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validate\Eloquent\ModelNotFoundException;
use illuminate\Validation\ValidationException as ValidationException;
use App\Models\Data;
use Exception;

class DataControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $Data = Data::query();
            $Data = $Data->paginate(3);

            $response=$Data;
            $code=200;
        } catch (Exception $e){
            $code=500;
            $response= $e->getMessage();
        }
        return apiResponseBuilder($code,$response); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'date' => 'required',
            'files' => 'required',
            'image' => 'required'
        ]);

        try {

            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

            $fileName = time().'.'.request()->files->getClientOriginalExtension();
            request()->files->move(public_path('file'), $fileName);

            $Data = new Data();

            $Data->name = $request->name;
            $Data->date = $request->date;
            $Data->files =  $fileName;
            $Data->image = $imageName;

            $Data->save();
            $code=200;
            $response=$Data;

        } catch (Exception $e){
            if($e instanceof ValidationException){
                $code = 400;
                $response = 'tidak ada data';
            } else {
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $Data = Data::findOrFail($id);
            
            $code=200;
            $response=$Data;

        } catch (Exception $e){
            if($e instanceof ModelNotFoundException){
                $code = 404;
                $response = 'inputkan sesuai id';
            } else {
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'date' => 'date'
        ]);

        try {
            $Data = Data::find($id);

            $Data->name = $request->name;
            $Data->date = $request->date;

            $Data->save();
            $code=200;
            $response=$Data;
        } catch (Exception $e){
            if ($e instanceof ValidationException){
                $code = 400;
                $response = 'data tidak ada';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $Data = Data::find($id);
            $Data->delete();
            $code=200;
            $response=$Data;
        } catch (Exception $e){
            $code=500;
            $response=$e->getMessage();
        }

        return apiResponseBuilder($code,$response);
    }
}
