<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::apiResource('/data', 'API\DataControllers');
Route::get('/data', 'API\DataControllers@index');
// // Membuat Artikel Baru
Route::post('/data', 'API\DataControllers@store');
// Route::post('/data', API\DataControllers::class . '@store');

Route::get('/data/{id}', 'API\DataControllers@show');
Route::put('/data/{id}', 'API\DataControllers@update');
Route::delete('/data/{id}', 'API\DataControllers@destroy');
